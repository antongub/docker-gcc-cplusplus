# C++ Docker Set Up
Docker setup to run c++ terminal applications
Tested on Linux Ubuntu 18.04

## Instruction
- Clone or Donwload this repository
- Adjust the CMakeLists.txt and build directory in the start.sh script
- run one of the execution commands from below


## Execute
### Docker build and execute from gitlab registry
`docker run --rm -it -v /path/to/project:/usr/application --name gcc-cplusplus registry.gitlab.com/antongub/docker-gcc-cplusplus:latest`

### Docker build and execute locally
1. Build: `docker build -t gcc-cplusplus:latest .`  
2. Execute: `docker run --rm -it -v /path/to/project:/usr/application --name gcc-cplusplus gcc-cplusplus:latest`

## Docker errors
If you get an error message with opening the `start.sh` file it is probably because you're on windows.  
So you firstly need to convert the `start.sh` Newlines CR-LF to Unix Format
